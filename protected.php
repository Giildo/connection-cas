<?php
preg_match('#(.*)\?.*#', $_SERVER['REQUEST_URI'], $matches, PREG_OFFSET_CAPTURE);
$uri = $matches[1] === null ? $_SERVER['REQUEST_URI'] : $matches[1][0];
var_dump($uri);
$url = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}{$uri}";
$urlEncoded = urlencode($url);

if ($_GET['ticket']) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "https://cas.unistra.fr/cas/serviceValidate?service={$urlEncoded}&ticket={$_GET['ticket']}");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    $response = curl_exec($curl);
    $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    echo $response;
} else {
    header("Location: https://cas.unistra.fr/cas/login?service={$urlEncoded}");
}
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1>Page protégée</h1>
</body>
</html>