<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        a {
            background-repeat: no-repeat;
            background-image: linear-gradient(180deg, rgba(0,0,0,0) 65%, #adffa5 0);
            line-height: 1.2;
            background-size: 0 100%;
            font-size: 25px;
            font-weight: 300;
            background-color: transparent;
            transition: background-size 0.4s ease;
        }

        a:hover {
            background-size: 100% 100%;
            transition: background-size 0.4s ease;
        }
    </style>
</head>
<body>
<a>Bonjour je suis un très long titre qui va sur potentilement plusieurs ligne !</a>

<button>Se connecter</button>

<script type="application/javascript">
document.querySelector('button')
        .addEventListener('click', ev => {
          document.location.href = `https://cas.unistra.fr/cas/login/?service=${window.location.href}`
        })

const ticket = window.location.search.match(/\?ticket=(ST-[0-9]+-[a-zA-Z0-9]+-cas[1|2])/)

if (ticket) {
  document.location.href = `https://cas.unistra.fr/cas/login/?ticket=${ticket[1]}`
}
</script>
</body>
</html>